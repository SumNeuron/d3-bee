(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('d3')) :
  typeof define === 'function' && define.amd ? define(['exports', 'd3'], factory) :
  (factory((global['d3-bee'] = global['d3-bee'] || {}),global.d3));
}(this, (function (exports,d3) { 'use strict';

  if (typeof document !== "undefined") {
    var element = document.documentElement;
  }

  function bee(container) {
    let namespace = 'bee',
        n = s => `${namespace}-${s}`,
        data,
        dataKeys,
        values,
        dataValues,
        valueExtractor = (key, index) => data[key],
        dataExtent,
        dataRadii,
        radiusExtractor = (key, index) => 5,
        scaleR = d3.scaleLinear().range([3, 10]),
        spaceX,
        spaceY,
        spaceRelax = 0,
        forceCollide = 4,
        simulation,
        sumulationTicks = 120,
        scaleX = d3.scaleLinear(),
        axisX,
        axisXTicks,
        gridlinesX,
        gAxisX,
        nodes,
        colorExtractor = (key, index) => {
      return d3.scaleSequential().interpolator(d3.interpolateViridis)(index / dataKeys.length);
    },
        stroke = (k, i) => colorExtractor(k, i),
        strokeWidth = 0.1,
        opacity = 0.5,
        transitionDuration = (k, i) => 500,
        easeFn = d3.easeSin,
        mouseover = (k, i) => {},
        mouseleave = (k, i) => {};

    bee.namespace = function (_) {
      return arguments.length ? (namespace = _, bee) : namespace;
    };

    bee.data = function (_) {
      return arguments.length ? (data = _, bee) : data;
    };

    bee.dataKeys = function (_) {
      return arguments.length ? (dataKeys = _, bee) : dataKeys;
    };

    bee.values = function (_) {
      return arguments.length ? (values = _, bee) : values;
    };

    bee.dataValues = function (_) {
      return arguments.length ? (dataValues = _, bee) : dataValues;
    };

    bee.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, bee) : valueExtractor;
    };

    bee.dataExtent = function (_) {
      return arguments.length ? (dataExtent = _, bee) : dataExtent;
    };

    bee.dataRadii = function (_) {
      return arguments.length ? (dataRadii = _, bee) : dataRadii;
    };

    bee.radiusExtractor = function (_) {
      return arguments.length ? (radiusExtractor = _, bee) : radiusExtractor;
    };

    bee.scaleR = function (_) {
      return arguments.length ? (scaleR = _, bee) : scaleR;
    };

    bee.spaceX = function (_) {
      return arguments.length ? (spaceX = _, bee) : spaceX;
    };

    bee.spaceY = function (_) {
      return arguments.length ? (spaceY = _, bee) : spaceY;
    };

    bee.spaceRelax = function (_) {
      return arguments.length ? (spaceRelax = _, bee) : spaceRelax;
    };

    bee.forceCollide = function (_) {
      return arguments.length ? (forceCollide = _, bee) : forceCollide;
    };

    bee.simulation = function (_) {
      return arguments.length ? (simulation = _, bee) : simulation;
    };

    bee.sumulationTicks = function (_) {
      return arguments.length ? (sumulationTicks = _, bee) : sumulationTicks;
    };

    bee.scaleX = function (_) {
      return arguments.length ? (scaleX = _, bee) : scaleX;
    };

    bee.axisX = function (_) {
      return arguments.length ? (axisX = _, bee) : axisX;
    };

    bee.axisXTicks = function (_) {
      return arguments.length ? (axisXTicks = _, bee) : axisXTicks;
    };

    bee.gridlinesX = function (_) {
      return arguments.length ? (gridlinesX = _, bee) : gridlinesX;
    };

    bee.gAxisX = function (_) {
      return arguments.length ? (gAxisX = _, bee) : gAxisX;
    };

    bee.nodes = function (_) {
      return arguments.length ? (nodes = _, bee) : nodes;
    };

    bee.colorExtractor = function (_) {
      return arguments.length ? (colorExtractor = _, bee) : colorExtractor;
    };

    bee.stroke = function (_) {
      return arguments.length ? (stroke = _, bee) : stroke;
    };

    bee.strokeWidth = function (_) {
      return arguments.length ? (strokeWidth = _, bee) : strokeWidth;
    };

    bee.opacity = function (_) {
      return arguments.length ? (opacity = _, bee) : opacity;
    };

    bee.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, bee) : transitionDuration;
    };

    bee.easeFn = function (_) {
      return arguments.length ? (easeFn = _, bee) : easeFn;
    };

    bee.mouseover = function (_) {
      return arguments.length ? (mouseover = _, bee) : mouseover;
    };

    bee.mouseleave = function (_) {
      return arguments.length ? (mouseleave = _, bee) : mouseleave;
    }; // function tick() {
    //   container.selectAll(`circle.${n('node')}`)
    //     .attr('cx', d=>d.x)
    // 		.attr('cy', d=>d.y)
    // }


    function bee() {
      dataKeys = d3.keys(data);
      values = [];
      dataRadii = [];
      dataValues = dataKeys.map((k, i) => {
        values.push({
          key: k,
          value: data[k]
        });
        dataRadii.push(radiusExtractor(k, i));
        return valueExtractor(k, i);
      });
      scaleR.domain(d3.extent(dataRadii));
      dataExtent = d3.extent(dataValues);
      let [dataMin, dataMax] = dataExtent;
      scaleX.range([0 + spaceRelax, spaceX - 2 * spaceRelax]).domain([dataMin, dataMax]);
      axisX = d3.axisBottom(scaleX).ticks(axisXTicks);
      if (gridlinesX) axisX.tickSize(-spaceY);
      gAxisX = container.select('g.x-axis');
      if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis');
      gAxisX.transition(transitionDuration).ease(easeFn).attr('transform', `translate(${0}, ${spaceY})`).call(axisX);
      simulation = d3.forceSimulation(values).force('x', d3.forceX(function (d, i) {
        return scaleX(valueExtractor(d.key, i));
      }).strength(1)).force('y', d3.forceY(spaceY / 2)).force('collide', d3.forceCollide(forceCollide)).stop();

      for (var i = 0; i < sumulationTicks; ++i) simulation.tick();

      let gNodes = container.select(`g.${n('nodes-container')}`);
      if (gNodes.empty()) gNodes = container.append('g').attr('class', `${n('nodes-container')}`);
      let voroni = d3.voronoi().extent([[0 - spaceRelax, 0 - spaceRelax], [spaceX + spaceRelax, spaceY + spaceRelax]]).x(d => d.x).y(d => d.y).polygons(values);
      nodes = gNodes.selectAll(`circle.${n('nodes')}`).data(voroni);
      nodes.exit().remove();
      nodes = nodes.merge(nodes.enter().append('circle').attr('class', `${n('nodes')}`));
      nodes.transition((d, i) => {
        if (d === undefined) return;
        return transitionDuration(d.data.key, i);
      }).ease(easeFn).attr('r', (d, i) => {
        if (d === undefined) return 0;
        return scaleR(radiusExtractor(d.data.key, i));
      }).attr('cx', (d, i) => {
        if (d === undefined) return 0;
        return d.data.x;
      }).attr("cy", (d, i) => {
        if (d === undefined) return 0;
        return d.data.y;
      }).attr('fill', (d, i) => {
        if (d === undefined) return 'transparent';
        return colorExtractor(d.data.key, i);
      }).attr("stroke", (d, i) => {
        if (d === undefined) return 'transparent';
        return stroke(d.data.key, i);
      }).attr("stroke-width", strokeWidth).attr('opacity', opacity);
      nodes.on('mouseenter', mouseover).on('mouseover', mouseover).on('mouseleave', mouseleave).on('mouseexit', mouseleave);
    }

    return bee;
  }

  function getTranslation(transform) {
    var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    transform = transform == undefined ? 'translate(0,0)' : transform;
    g.setAttributeNS(null, 'transform', transform);
    var matrix = g.transform.baseVal.consolidate().matrix;
    return [matrix.e, matrix.f];
  }
  function resizeDebounce(f, wait) {
    var resize = debounce(function () {
      f();
    }, wait);
    window.addEventListener('resize', resize);
  }
  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;

      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  var utils = /*#__PURE__*/Object.freeze({
    getTranslation: getTranslation,
    resizeDebounce: resizeDebounce,
    debounce: debounce
  });

  const d3_bee = {
    bee,
    utils
  };

  if (typeof window !== 'undefined') {
    window.d3_bee = d3_bee;
  }

  exports.default = d3_bee;
  exports.bee = bee;
  exports.utils = utils;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
