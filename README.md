# d3-bee
A simple bee-swarm plot


# Install

```
npm i d3-bee
```

# Usage

```js
import {bee} from 'd3-bee'
```

## Assumptions

It is assumed that the `data` bound to `bee` is an object, where key corresponds
to a "bee" in the swarm

```js
data = {
  'A': 10,
  'B': 20,
  ...
}
```

more complex data can be provided should utility functions such as `valueExtractor`
be overwritten from their default behavior:

```js
data = {
  A: {
    x: { value: 1 },
    y: { value: 2 },
    ...
  },
  ...
}
```
