import { event, mouse} from 'd3-selection';

import bee from './modules/bee'
import * as utils from './modules/utils'


const d3_bee = {
  bee, utils
}

if (typeof window !== 'undefined') {
  window.d3_bee = d3_bee;
}


export default d3_bee
export {bee, utils}
